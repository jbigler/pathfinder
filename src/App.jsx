import React, { Component } from 'react';
import logo from './d20.svg';
import './App.css';
import { Navbar, Jumbotron, Button } from 'react-bootstrap';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Pathfinder Character</h1>
        </header>
        <div className="container-float">
        
          {/* this is the title section */}
          <div className="row">
            <div className="col-md-1 spacer"></div>
            <h3 className="col-md-2 sectiontitle">Stats</h3>
            <div className="col-md-1 spacer"></div>
            <h3 className="col-md-3 sectiontitle">Offense</h3>
            <div className="col-md-1 spacer"></div>
            <h3 className="col-md-3 sectiontitle">Defense</h3>
            <div className="col-md-1 spacer"></div>
          </div>

          {/*This is the ui for the stats group  */}
          <form className="form-horizontal col-md-4"  role="form" id="stats">
            <div className="statsgroup">   
              <div className="form-group" id="stats">
                <label for="lvl" className="control-label col-md-4">Level:</label>
                  <div className="col-md-2">
                    <input id="lvl" type="number" className="form-control" />
                  </div>
              </div>

              <div className="form-group" id="stats">
                <label for="str" className="control-label col-md-4">Str:</label>
                  <div className="col-md-2">
                    <input id="str" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group" id="stats">
                <label for="dex" className="control-label col-md-4">Dex:</label>
                  <div className="col-md-2">
                    <input id="dex" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="con" className="control-label col-md-4">Con:</label>
                  <div className="col-md-2">
                    <input id="con" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="int" className="control-label col-md-4">Int:</label>
                  <div className="col-md-2">
                    <input id="int" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="wis" className="control-label col-md-4">Wis:</label>
                  <div className="col-md-2">
                    <input id="wis" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="cha" className="control-label col-md-4">Cha:</label>
                  <div className="col-md-2">
                    <input id="cha" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="hp" className="control-label col-md-4">Hp:</label>
                  <div className="col-md-2">
                    <input id="hp" type="number" className="form-control input-sm" />
                  </div>
              </div>

              <div className="form-group">
                <label for="bab" className="control-label col-md-4">Base Attack:</label>
                  <div className="col-md-2">
                    <input id="bab" type="number" className="form-control input-sm"/>
                  </div>
              </div>

              <div className="form-group">
                <label for="fort" className="control-label col-md-4">Fort:</label>
                  <div className="col-md-2">
                    <input id="fort" type="number" className="form-control input-sm"/>
                  </div>
              </div>

              <div className="form-group">
                <label for="ref" className="control-label col-md-4">Ref:</label>
                  <div className="col-md-2">
                    <input id="ref" type="number" className="form-control input-sm"/>
                  </div>
              </div>

              <div className="form-group">
                <label for="wil" className="control-label col-md-4">Will:</label>
                  <div className="col-md-2">
                    <input id="wil" type="number" className="form-control input-sm"/>
                  </div>
              </div>

              <div className="form-group">
                <label for="weaponhandedness" className="control-label col-md-4">Weapon:</label>
                  <div className="col-md-2">
                    <select id="weaponhandedness" className="form-control input-sm">
                      <option selected>1 hand</option>
                      <option selected>2 hand</option>
                      <option selected>DW</option>
                    </select>
                  </div>
              </div>

            </div>

            {/* This is the UI for adding a new modifier */}
            <form>
              <div className="form-row"> 
                <button onClick="someFunction()">Add Modifier</button>
                <button onClick="someOtherFunction()">Add More Rows</button>
              </div>                    
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label for="source">Source (feat, etc...)</label>
                  <select className="form-control" id="source">
                    <option selected>Feat</option>
                    <option>Magic item</option>
                    <option>Trait</option>
                    <option>Racial</option>
                    <option>Class Feature</option>
                    <option>Spell</option>
                    <option>Condition</option>
                    <option>Mundane Item</option>
                    <option>Other</option>                    
                  </select>
                </div>
                <div className="form-group col-md-6">
                  <label for="sourcedescription">Description</label>
                  <input type="text" className="form-control" id="sourcedescription" placeholder="Power Attack"/>
                </div>                                
              </div>
              <div className="form-row">                  
                <div className="form-group col-md-2">
                  <label for="amount">#</label>
                  <input type="number" className="form-control" id="amount"/>
                </div>
                <div className="form-group col-md-4">
                  <label for="modifies">Modifies</label>
                  <select id="modifies" className="form-control">
                    <option selected>To hit</option>
                    <option>Damage</option>
                    <option>Str</option>
                    <option>Dex</option>
                    <option>Con</option>
                    <option>Int</option>
                    <option>Wis</option>
                    <option>Cha</option>
                    <option>Initiative</option>
                    <option>CMB</option>
                    <option>HP</option>
                    <option>AC</option>
                    <option>DR</option>
                    <option>CMD</option>
                    <option>Fort</option>
                    <option>Ref</option>
                    <option>Will</option>
                  </select>
                </div>
                <div className="form-group col-md-4">
                  <label for="type">Type</label>
                  <select className="form-control" id="type">
                    <option selected>Choose. . . </option>
                    <option>Luck</option>
                    <option>Profane</option>
                    <option>Circumstance</option>
                    <option>Magic</option>
                    <option>Morale</option>
                    <option>Enhancement</option>
                    <option>Untyped</option>
                  </select>
                </div>
              </div>
            </form>    
          </form>

          {/*This is the UI for the Offense section*/}
          <div className="col-md-3">
            <div className="row offense">
                  <div className="col-md-8">To Hit</div>
                  <div className="tohit col-md-4">10</div>               
            </div>
            <div className="row offense">
                  <div className="col-md-8">Damage</div>
                  <div className="todamage col-md-4">10</div>               
            </div>
            <div className="row offense">
                  <div className="col-md-8">D. Dice</div>
                  <div className="damagedice col-md-4">2D8</div>               
            </div>

            <div className="row offense">
                  <div className="col-md-8">CMB</div>
                  <div className="cmb col-md-4">10</div>               
            </div>

            <div className="row listModifiers">
                  <div className="col-md-6">Power Attack</div>
                  <div className="col-md-1 spacer"/>
                  <button className="col-md-2" onClick="someFuction()">Toggle</button>
                  <div className="col-md-1 spacer"/>
                  <button className="col-md-2" onClick="someFunction()">View</button>
            </div>
          </div>

          {/*This is a spacer between the Offense and Defense section. Don't know if there's a better way to go about that */}
          <div className="col-md-1 spacer"> </div>

          {/*This is the UI section for defense */}
          <div className="col-md-4">
            <div className="col-md-4">
              <div className="row defense">
                    <div className="col-md-8">HP</div>
                    <div className="tohit col-md-4">94</div>               
              </div>
              <div className="row defense">
                    <div className="col-md-8">AC</div>
                    <div className="todamage col-md-4">36</div>               
              </div>
              <div className="row defense">
                    <div className="col-md-8">Touch</div>
                    <div className="col-md-4">18</div>               
              </div>
              <div className="row defense">
                    <div className="col-md-8">DR</div>
                    <div className="damagedice col-md-4">5</div>               
              </div>
              <div className="row defense">
                    <div className="col-md-8">CMD</div>
                    <div className="cmb col-md-4">35</div>                   
              </div>
              <div className="row defense">
                    <div className="col-md-8">Fort</div>
                    <div className="cmb col-md-4">12</div>                   
              </div>
              <div className="row defense">
                    <div className="col-md-8">Ref</div>
                    <div className="cmb col-md-4">11</div>                   
              </div>
              <div className="row defense">
                    <div className="col-md-8">Will</div>
                    <div className="cmb col-md-4">12</div>                   
              </div>
            </div>
          </div>

      </div> 
    </div>
    );
  }
}

export default App;
